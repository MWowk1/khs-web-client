app.controller("homeController", function($scope){

	$scope.showRightMenu = function(){
		$('#rightMenu').stop(true,true).show('slide', { direction: 'left' });
	};

	$scope.hideRightMenu = function(){
		$('#rightMenu').stop(true,true).hide('slide', { direction: 'left' });
	};

	$scope.showLeftMenu = function(){
		$('#leftMenu').stop(true,true).show('slide', { direction: 'right' });
	};

	$scope.hideLeftMenu = function(){
		$('#leftMenu').stop(true,true).hide('slide', { direction: 'right' });
	};

	$scope.scrollDown = function(){
		$("html, body").animate({ scrollTop: $('#home-header').height() }, "slow");
	};

	$scope.frontNavItemsClick = function(){
		event = event || window.event;
	    event.stopImmediatePropagation();
	};

	$scope.fbHover = function(){
		$('#fbIcon').find('img').attr("src","/assets/img/icons/fb-black.svg");
	};
	$scope.fbNormal = function(){
		$('#fbIcon').find('img').attr("src","/assets/img/icons/fb.svg");
	};
	$scope.igHover = function(){
		$('#igIcon').find('img').attr("src","/assets/img/icons/ig-black.svg");
	};
	$scope.igNormal = function(){
		$('#igIcon').find('img').attr("src","/assets/img/icons/ig.svg");
	};

	$scope.searchFocus = function(){
		$('#home-header').css('background', "url('/assets/img/bg-min.jpg') no-repeat center center");
		$('#home-header').css('background-size', "cover");
	};

	function hideMenus(){
		$scope.hideLeftMenu();
		$scope.hideRightMenu();
	};

	$(function(){
		$('#header').css('display', 'none');
	})
	$(window).load(function(){
		$('html').click(function(event) {
			var parentId = event.target.parentNode.id;//IE Support
			if(parentId != 'frontNavItems'){
				hideMenus();
			}
		});
		$(window).scroll(function(){
			if($(this).scrollTop() > $(this).height() / 3){
				$('#rightMenu').fadeOut('slow');
				$('#leftMenu').fadeOut('slow');
			}
		});
	});
});