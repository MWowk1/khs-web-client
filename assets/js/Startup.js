var animating = false;
var bg = 0;

$(function(){
	//block the screen when the DOM is loaded

	$.blockUI.defaults.css = { 
        padding: 0,
        margin: 0,
        width: '30%',
        top: '40%',
        left: '35%',
        textAlign: 'center',
        cursor: 'pointer'
    };
    $.blockUI({ 
        message: '<img src="/assets/img/loader.svg" />', 
        css: { 
            top:  ($(window).height() - 75) /2 + 'px', 
            left: ($(window).width() - 75) /2 + 'px', 
            width: '75px' 
        } 
    }); 
});

$(window).load(function(){

	//unblock the screen when the page is fully loaded
	setTimeout($.unblockUI,1500);
	$.notify("This website is still in beta.", "info");

});