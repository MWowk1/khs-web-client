var app = angular.module("KHSNav", ["ngRoute"])
	.config(function($routeProvider, $locationProvider){
		$routeProvider
			.when("/home", {
				templateUrl:"templates/home.html",
				controller:"homeController"
			})
			.when("/halloffame/athletes", {
				templateUrl:"templates/halloffame.html",
				controller:"hallOfFame"
			})
			.when("/halloffame/artists", {
				templateUrl:"templates/halloffame.html",
				controller:"hallOfFame"
			})
			.when("/halloffame/entrepreneurs", {
				templateUrl:"templates/halloffame.html",
				controller:"hallOfFame"
			})
			.when("/halloffame/intellectuals", {
				templateUrl:"templates/halloffame.html",
				controller:"hallOfFame"
			})
			.when("/halloffame/officials", {
				templateUrl:"templates/halloffame.html",
				controller:"hallOfFame"
			})
			.when("/halloffame/writers", {
				templateUrl:"templates/halloffame.html",
				controller:"hallOfFame"
			})
			.otherwise({
				redirectTo:"/home"
			})
		$locationProvider.html5Mode(true);
});