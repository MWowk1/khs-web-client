app.controller("hallOfFame", function($scope){
	$(function(){
		$('#header').css('display', 'block');
	})
});

app.controller("hallOfFameAthletes", function($scope){
	$scope.message = "Athletes";
});

app.controller("hallOfFameArtists", function($scope){
	$scope.message = "Artists";
});

app.controller("hallOfFameEntrepreneurs", function($scope){
	$scope.message = "Entrepreneurs and Bankers";
});

app.controller("hallOfFameIntellectuals", function($scope){
	$scope.message = "Intellectuals and Educators";
});

app.controller("hallOfFameOfficials", function($scope){
	$scope.message = "Politicians, Lawyers, and Civil Servants";
});

app.controller("hallOfFameWriters", function($scope){
	$scope.message = "Writers and Journalists";
});